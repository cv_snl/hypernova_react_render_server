const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: {
        consumer: './client/src/consumer',
        industry: './client/src/industry'
    },
    target: 'node',
    externals: [nodeExternals()],
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, 'ssr'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
};
