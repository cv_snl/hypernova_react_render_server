const hypernova = require('hypernova/server');
const { renderReact } = require('hypernova-react');

const bundles = {
  consumer: require('./ssr/consumer.bundle'),
  industry: require('./ssr/industry.bundle')
}

hypernova({
  devMode: true,

  getComponent(name) {
    const [bundle, component] = name.split('_');

    for (let componentName in bundles[bundle]) {
      if (component === componentName) {
        return renderReact(componentName, bundles[bundle][componentName]);
      }
    }

    return null;
  },
  logger: {},
  host: '0.0.0.0',
  port: 3030,
});
