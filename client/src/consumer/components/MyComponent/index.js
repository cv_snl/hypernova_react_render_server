import React from "react";

const MyComponent = props => (
  <div className="CONSUMER_TEST">Consumer, { props.name }</div>
);

export default MyComponent;
