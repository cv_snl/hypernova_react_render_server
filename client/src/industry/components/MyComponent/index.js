import React from "react";

const MyComponent = props => (
  <div className="CONSUMER_TEST">Industry, { props.name }</div>
);

export default MyComponent;